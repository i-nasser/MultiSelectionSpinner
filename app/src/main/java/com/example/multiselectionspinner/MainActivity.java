package com.example.multiselectionspinner;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;

import com.example.multiselectionspinner.Models.ItemModel;
import com.example.multiselectionspinner.Utility.MultiSelectionSpinner;

import java.util.ArrayList;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        MultiSelectionSpinner multiSelectionSpinner = findViewById(R.id.spinnerItem);

        ArrayList<ItemModel> excludeModels = new ArrayList<>();
        excludeModels.add(new ItemModel(1,"Cola"));
        excludeModels.add(new ItemModel(2,"Potato ships"));
        excludeModels.add(new ItemModel(3,"Coleslow"));
        excludeModels.add(new ItemModel(4,"e"));
        excludeModels.add(new ItemModel(5,"f"));
        excludeModels.add(new ItemModel(6,"g"));

        multiSelectionSpinner.setItems(excludeModels);

        // To set selected items
//        ArrayList<ExcludeModel> setItemsSelected = new ArrayList<>();
//        setItemsSelected.add(new ExcludeModel(6,"g","g"));
//        multiSelectionSpinner.setSelection(setItemsSelected);


        // To get the selected Item list
//        ArrayList<ExcludeModel> selectedItems = multiSelectionSpinner.getSelectedItems();
//        for (int i = 0; i < selectedItems.size(); i++) {
//            Toast.makeText(this, ""+selectedItems.get(i).getExcludeEn(), Toast.LENGTH_SHORT).show();
//        }
    }
}
