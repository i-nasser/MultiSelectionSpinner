package com.example.multiselectionspinner.Utility;

import android.annotation.SuppressLint;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.util.AttributeSet;
import android.widget.ArrayAdapter;
import android.widget.SpinnerAdapter;
import android.widget.Toast;
import androidx.appcompat.widget.AppCompatSpinner;
import com.example.multiselectionspinner.Models.ItemModel;
import com.example.multiselectionspinner.R;
import java.util.ArrayList;
import java.util.Arrays;

public class MultiSelectionSpinner extends AppCompatSpinner implements DialogInterface.OnMultiChoiceClickListener {

    private ArrayList<ItemModel> items = null;
    private boolean[] selection = null;
    private ArrayAdapter adapter;
    Context context;

    public MultiSelectionSpinner(Context context) {
        super(context);

        adapter = new ArrayAdapter(context, android.R.layout.simple_spinner_item);
        super.setAdapter(adapter);
    }

    public MultiSelectionSpinner(Context context, AttributeSet attrs) {
        super(context, attrs);
        this.context = context;
        adapter = new ArrayAdapter(context, android.R.layout.simple_spinner_item);
        super.setAdapter(adapter);
    }

    @Override
    public void onClick(DialogInterface dialog, int idx, boolean isChecked) {
        if (selection != null && idx < selection.length) {
            selection[idx] = isChecked;

            adapter.clear();
            adapter.add(buildSelectedItemString());
        } else {
            throw new IllegalArgumentException("'idx' is out of bounds.");
        }
    }

    @SuppressLint("ClickableViewAccessibility")
    @Override
    public boolean performClick() {
        final AlertDialog.Builder builder = new AlertDialog.Builder(getContext());
        String[] itemNameExcludes = new String[items.size()];

        for (int i = 0; i < items.size(); i++) {
            itemNameExcludes[i] = items.get(i).getName();
        }

        builder.setMultiChoiceItems(itemNameExcludes, selection, this);

        builder.setPositiveButton(context.getString(R.string.ok), new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface arg0, int arg1) {
                ArrayList<ItemModel> selectedItems = getSelectedItems();
                for (int i = 0; i < selectedItems.size(); i++) {
                    Toast.makeText(context, ""+selectedItems.get(i).getName(), Toast.LENGTH_SHORT).show();
                }
            }
        }).setNegativeButton(context.getString(R.string.close), new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
            }
        });

        builder.show();

        return true;
    }


    @Override
    public void setAdapter(SpinnerAdapter adapter) {
        throw new RuntimeException("setAdapter is not supported by MultiSelectSpinner.");
    }

    public void setItems(ArrayList<ItemModel> items) {
        this.items = items;
        selection = new boolean[this.items.size()];
        adapter.clear();
        adapter.add("");
        Arrays.fill(selection, false);
    }

    public void setSelection(ArrayList<ItemModel> selection) {
        for (int i = 0; i < this.selection.length; i++) {
            this.selection[i] = false;
        }

        for (ItemModel sel : selection) {
            for (int j = 0; j < items.size(); ++j) {
                if (items.get(j).getName().equals(sel.getName())) {
                    this.selection[j] = true;
                }
            }
        }

        adapter.clear();
        adapter.add(buildSelectedItemString());
    }


    private String buildSelectedItemString() {
        StringBuilder sb = new StringBuilder();
        boolean foundOne = false;

        for (int i = 0; i < items.size(); ++i) {
            if (selection[i]) {
                if (foundOne) {
                    sb.append(", ");
                }

                foundOne = true;

                sb.append(items.get(i).getName());
            }
        }

        return sb.toString();
    }


    public ArrayList<ItemModel> getSelectedItems() {
        ArrayList<ItemModel> selectedItems = new ArrayList<>();

        for (int i = 0; i < items.size(); ++i) {
            if (selection[i]) {
                selectedItems.add(items.get(i));
            }
        }

        return selectedItems;
    }
}
