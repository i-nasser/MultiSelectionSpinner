package com.example.multiselectionspinner.Models;

public class ItemModel {

    private int id;
    private String name;

    public ItemModel(int id, String name) {
        this.id = id;
        this.name = name;
    }

    public int getId() {
        return id;
    }

    public String getName() {
        return name;
    }
}
